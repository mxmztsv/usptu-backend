const {toRU} = require("../services/dateService");

test('date to RU format', () => {
	expect(toRU('2000-01-23')).toBe('23.01.2000')
})
