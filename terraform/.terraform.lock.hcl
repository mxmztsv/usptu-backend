# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/sbercloud-terraform/sbercloud" {
  version = "1.11.6"
  hashes = [
    "h1:2Msq5dgBgv3MaInn0dCQEMOuG5PgtcYrl3doEx5Xe0Q=",
  ]
}
